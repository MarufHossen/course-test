import 'package:course_test/network/random_number_api.dart';

class RandomNumberController{
  dynamic getRandomNumber(){
    return RandomNumberApi.fetchRandomNumber();
  }
}